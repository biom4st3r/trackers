package com.biom4st3r.trackers;

import java.util.List;
import java.util.Random;
import java.util.function.Supplier;

import net.fabricmc.fabric.api.renderer.v1.model.FabricBakedModel;
import net.fabricmc.fabric.api.renderer.v1.render.RenderContext;
import net.minecraft.block.BlockState;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.model.BakedModel;
import net.minecraft.client.render.model.BakedQuad;
import net.minecraft.client.render.model.json.ModelOverrideList;
import net.minecraft.client.render.model.json.ModelTransformation;
import net.minecraft.client.texture.Sprite;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.BlockRenderView;

/**
 * TrackerItemModel
 */
public class TrackerItemModel implements BakedModel, FabricBakedModel {

    static BakedModel paper; // MinecraftClient.getInstance().getItemRenderer().getModels().getModel(Items.PAPER);
    public static final BakedModel INSTANCE = new TrackerItemModel();
    static void init()
    {
        paper = MinecraftClient.getInstance().getItemRenderer().getModels().getModel(Items.PAPER);
    }
    @Override
    public boolean isVanillaAdapter() {
        return false;
    }

    @Override
    public void emitBlockQuads(BlockRenderView blockView, BlockState state, BlockPos pos,
            Supplier<Random> randomSupplier, RenderContext context) {
    }

    @Override
    public void emitItemQuads(ItemStack stack, Supplier<Random> randomSupplier, RenderContext context) {
        if(paper == null) init();
        context.fallbackConsumer().accept(paper);
    }

    @Override
    public List<BakedQuad> getQuads(BlockState state, Direction face, Random random) {
        return null;
    }

    @Override
    public boolean useAmbientOcclusion() {
        return false;
    }

    @Override
    public boolean hasDepth() {
        return false;
    }

    @Override
    public boolean isSideLit() {
        return false;
    }

    @Override
    public boolean isBuiltin() {
        return false;
    }

    @Override
    public Sprite getSprite() {
        if(paper == null) init();
        return paper.getSprite();
    }

    @Override
    public ModelTransformation getTransformation() {
        if(paper == null) init();
        return paper.getTransformation();
    }

    @Override
    public ModelOverrideList getOverrides() {
        if(paper == null) init();
        return paper.getOverrides();
    }



    
}