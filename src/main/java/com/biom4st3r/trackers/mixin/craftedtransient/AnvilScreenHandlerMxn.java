package com.biom4st3r.trackers.mixin.craftedtransient;

import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.AnvilScreenHandler;
import net.minecraft.screen.ForgingScreenHandler;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.screen.ScreenHandlerType;

import com.biom4st3r.trackers.components.PooledItemComponent;
import com.biom4st3r.trackers.components.TrackerComponent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

@Mixin(AnvilScreenHandler.class)
public abstract class AnvilScreenHandlerMxn extends ForgingScreenHandler {

    public AnvilScreenHandlerMxn(ScreenHandlerType<?> type, int syncId, PlayerInventory playerInventory,
            ScreenHandlerContext context) {
        super(type, syncId, playerInventory, context);
    }

    /**
     * Collect all ITrackerComponents from input items and output items and getPreferedComponenets from all to be added to output
     * @param ci
     */
    @Inject(
        at = @At(
            value = "INVOKE", 
            target = "net/minecraft/screen/AnvilScreenHandler.sendContentUpdates()V", 
            ordinal = -1, 
            shift = Shift.BEFORE), 
        method = "updateResult", 
        cancellable = false, 
        locals = LocalCapture.NO_CAPTURE)
    private void transferComponent(CallbackInfo ci) {
        ItemStack input0 = this.input.getStack(0);
        ItemStack input1 = this.input.getStack(1);
        if (!this.player.world.isClient && !input0.isEmpty() && !input1.isEmpty()) {
            ItemStack output = this.output.getStack(0);
            if(output.isEmpty()) return;

            PooledItemComponent.borrowAllComponentsOfSubType(TrackerComponent.class, input0, l->l.forEach(c->c.toStack(output)), false);
            PooledItemComponent.borrowAllComponentsOfSubType(TrackerComponent.class, input1, l->l.forEach(c->c.toStack(output)), false);
        }
    }
}