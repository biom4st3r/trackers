package com.biom4st3r.trackers.mixin.craftedtransient;

import java.util.List;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.CraftingInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.slot.CraftingResultSlot;
import net.minecraft.screen.slot.Slot;
import net.minecraft.util.Util;

import com.biom4st3r.trackers.components.CraftedComponent;
import com.google.common.collect.Lists;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

/**
 * CratingResultSlot
 */
@Mixin(CraftingResultSlot.class)
public abstract class CraftingResultSlotMxn extends Slot {

    public CraftingResultSlotMxn(Inventory inventory, int index, int x, int y) {
        super(inventory, index, x, y);
    }

    @Shadow @Final
    private CraftingInventory input;

    @Shadow
    @Final
    private  PlayerEntity player;

    // BioLogger l = new BioLogger("CratingResultSlotMxn");
    
    /**
     * Collect all ITrackerComponents from input items and output items and getPreferedComponenets from all to be added to output
     * @param player
     * @param outputStack
     * @param ci
     */
    @Inject(
        at = @At("HEAD"), 
        method = "onTakeItem", 
        cancellable = false,
        locals = LocalCapture.NO_CAPTURE)
    private void onTakeItemApplyComponent(PlayerEntity player, ItemStack outputStack, CallbackInfo ci)
    {
        if(!player.world.isClient) biom4s4t3r_ApplyTrackerComponentFromInput(outputStack);
    }

    @Unique
    private void biom4s4t3r_ApplyTrackerComponentFromInput(ItemStack outputStack)
    {
        List<ItemStack> stacks = Util.make(Lists.newArrayList(),(list)->
        {
            for(int i = 0; i < input.size();i++) if(!input.getStack(i).isEmpty()) list.add(input.getStack(i));
        });

        CraftedComponent.KEY.borrowPooledComponent(outputStack, component-> {
            boolean shouldSetName = true;
            for(ItemStack stack : stacks) {
                if(stack.getItem() == outputStack.getItem()) {
                    shouldSetName = false;
                    break;
                }
            }
            if(shouldSetName) component.setOwner(player.getEntityName()); // Probably crafting a new item. Initlize the creator
            else if (stacks.size() == 2 && stacks.get(0).getItem() == outputStack.getItem() && stacks.get(1).getItem() == outputStack.getItem()) {
                // if a repair recipe: try to transfer to the output
                CraftedComponent.KEY.borrowPooledComponents(stacks, list-> {
                    for(CraftedComponent c : list) {
                        if(c.hasOwner()) {
                            c.toStack(outputStack);
                            break;
                        }
                    }
                }, false);
            }
        }, true);

        // ITrackerComponent.getPreferedComponents(stacks.toArray(ItemStack[]::new)).forEach((type,comp)->
        // {
        //     // Optional<? extends ITrackerComponent> optional = type.maybeGet(outputStack);
        //     type.maybeGet(outputStack).ifPresent((outputComp)->
        //     {
        //         outputComp.fromTag(comp.toTag(new NbtCompound()));
        //         if(type == CraftedComponent.Crafted_Component) {
        //             boolean shouldSetName = true;
        //             for(int i = 0; i < input.size(); i++) {
        //                 if(input.getStack(i).getItem() == outputStack.getItem())
        //                 {
        //                     shouldSetName = false;
        //                 }
        //             }
        //             if(shouldSetName) ((CraftedComponent)outputComp).setOwner(player.getEntityName());
        //         }
        //     });
        // });
    }

    /**
     * Let the output be displayed with provide componenets.
     * getStack is never called to get the item from the CraftingResultSlot, so this is purely Cosmetic
     */
    @Override
    public void setStack(ItemStack outputStack) {
        // Purely Cosmetic
        biom4s4t3r_ApplyTrackerComponentFromInput(outputStack);
        super.setStack(outputStack);
    }
    
    /**
     * For quickcraft(shift craft) ScreenHandler.method_30010 else and QUICK_MOVE
     */
    @Override
    public ItemStack getStack() {
        ItemStack outputStack = super.getStack();
        if(!player.world.isClient) biom4s4t3r_ApplyTrackerComponentFromInput(outputStack);
        return outputStack;
    }
}