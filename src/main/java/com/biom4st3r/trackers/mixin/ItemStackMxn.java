package com.biom4st3r.trackers.mixin;

import java.util.List;

import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;

import net.fabricmc.loader.api.FabricLoader;

import com.biom4st3r.trackers.components.PooledItemComponent;
import com.biom4st3r.trackers.components.TrackerComponent;
import com.biom4st3r.trackers.components.PooledItemComponent.ComponentKey;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

/**
 * ItemStackMxn
 * Adding the Tooltips from Components
 */
@Mixin(ItemStack.class)
public class ItemStackMxn {

    @Shadow @Final
    private Item item;

    /**
     * Add Tool Tips from tracker components
     * @param player
     * @param context
     * @param ci
     * @param list
     */
    @Inject(
        method = "getTooltip",
        at = @At(
            value = "INVOKE",
            shift = Shift.BEFORE,
            target = "net/minecraft/item/ItemStack.appendEnchantments(Ljava/util/List;Lnet/minecraft/nbt/NbtList;)V",// target = "java/util/List.add(Ljava/lang/Object;)Z",
            ordinal = 0),
        locals = LocalCapture.CAPTURE_FAILEXCEPTION)
    public void getTooltipWithComponents(PlayerEntity player, TooltipContext context, CallbackInfoReturnable<List<Text>> ci, List<Text> list)
    {
        ItemStack stack = (ItemStack)(Object)this;

        if(FabricLoader.getInstance().isDevelopmentEnvironment()) list.add(new LiteralText("-------------"));
        for(ComponentKey<? extends TrackerComponent> i : PooledItemComponent.getSubTypeOf(TrackerComponent.class)) {
            i.borrowPooledComponent(stack, component-> {
                for(Text text : component.getToolTips()) {
                    list.add(text);
                }
            }, false);
        }
    }

    // @Inject(
    //     at = @At("TAIL"), 
    //     method = "getTooltip", 
    //     cancellable = false,
    //     locals = LocalCapture.CAPTURE_FAILHARD)
    // public void q1q1q1q1(PlayerEntity player, TooltipContext context, CallbackInfoReturnable<List<Text>> ci, List<Text> list)
    // {
    //     ComponentProvider.fromItemStack((ItemStack)(Object)this).getComponentContainer().keys().forEach((key)->
    //     {
    //         list.add(new LiteralText(key.getId().toString()).formatted(Formatting.GOLD));
    //     });;
    // }
    
}