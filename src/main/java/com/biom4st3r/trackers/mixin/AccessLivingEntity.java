package com.biom4st3r.trackers.mixin;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.loot.context.LootContext;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;
import org.spongepowered.asm.mixin.gen.Invoker;

@Mixin({LivingEntity.class})
public interface AccessLivingEntity {
    @Invoker("getLootContextBuilder")
    LootContext.Builder invoke_getLootContextBuilder(boolean causedByPlayer, DamageSource source);
    @Accessor("playerHitTimer")
    int getPlayerHitTimer();
}
