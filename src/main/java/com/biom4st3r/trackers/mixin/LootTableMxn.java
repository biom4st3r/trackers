package com.biom4st3r.trackers.mixin;

import java.util.function.Consumer;

import com.biom4st3r.trackers.components.FoundLootComponent;
import org.objectweb.asm.Opcodes;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyVariable;
import org.spongepowered.asm.mixin.injection.Slice;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.item.ItemStack;
import net.minecraft.loot.LootTable;
import net.minecraft.loot.context.LootContext;

@Mixin({LootTable.class})
public class LootTableMxn {

    private ThreadLocal<LootContext> context = new ThreadLocal<>();

    @Inject(
        at = @At(
            value = "JUMP", 
            opcode = Opcodes.IFEQ,
            ordinal = 0,
            shift = Shift.BY,
            by = 1
        ),
        slice = @Slice(
            from = @At(value = "INVOKE", target = "net/minecraft/loot/context/LootContext.markActive(Lnet/minecraft/loot/LootTable;)Z"),
            to = @At(value = "FIELD", target = "net/minecraft/loot/LootTable.combinedFunction:Ljava/util/function/BiFunction;")
        ),
        method = "generateUnprocessedLoot")
    private void getContext(LootContext context, Consumer<ItemStack> consumer, CallbackInfo ci) {
        this.context.set(context);
    }
    
    @ModifyVariable(
        at = @At(
            value = "JUMP", 
            opcode = Opcodes.IFEQ,
            ordinal = 0,
            shift = Shift.BY,
            by = 2
        ),
        slice = @Slice(
            from = @At(value = "INVOKE", target = "net/minecraft/loot/context/LootContext.markActive(Lnet/minecraft/loot/LootTable;)Z"),
            to = @At(value = "FIELD", target = "net/minecraft/loot/LootTable.combinedFunction:Ljava/util/function/BiFunction;")
        ),
        method = "generateUnprocessedLoot")
    private Consumer<ItemStack> getContext(Consumer<ItemStack> consumer) {
        LootContext context = this.context.get();
        this.context.set(null);
        return is -> {
            FoundLootComponent.intercept(is, context);
            consumer.accept(is);
        };
    }
}
