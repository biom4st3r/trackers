package com.biom4st3r.trackers.mixin;

import com.biom4st3r.trackers.duck.LootContextExtras;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import net.minecraft.loot.context.LootContext;
import net.minecraft.loot.context.LootContextType;

@Mixin({LootContext.Builder.class}) 
public class LootContext_BuilderMxn {
    @Inject(at = @At("RETURN"),method = "build")
    private void addTypeToContext(LootContextType type, CallbackInfoReturnable<LootContext> ci) {
        ((LootContextExtras)ci.getReturnValue()).setType(type);
    }
}