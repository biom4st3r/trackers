package com.biom4st3r.trackers.mixin;

import java.util.List;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.stat.Stat;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import com.biom4st3r.trackers.components.PooledItemComponent;
import com.biom4st3r.trackers.components.UpdatingComponent;
import com.biom4st3r.trackers.components.PooledItemComponent.ComponentKey;
import com.mojang.authlib.GameProfile;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

/** 
 * ServerPlayerEntityMxn
 * For listening to stats to update the tools
 */
@Mixin(ServerPlayerEntity.class)
public abstract class ServerPlayerEntityMxn extends PlayerEntity {
    public ServerPlayerEntityMxn(World world, BlockPos blockPos, float f, GameProfile gameProfile) {
        super(world, blockPos, f, gameProfile);
    }

    /**
     * Increment trackers components on all equiped items
     * @param stat
     * @param amount
     * @param ci
     */
    @Inject(method = "increaseStat", at = @At("HEAD"))
    public void increaseStat(Stat<?> stat, int amount,CallbackInfo ci) {
        List<ComponentKey<? extends UpdatingComponent>> types = PooledItemComponent.getSubTypeOf(UpdatingComponent.class);
        for(ItemStack is : this.getItemsEquipped()) {
            for(ComponentKey<? extends UpdatingComponent> x : types) {
                x.getComponent(is).ifPresent(c->c.release(c.react(stat, amount)));
            }
        }
    }
}