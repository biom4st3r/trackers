package com.biom4st3r.trackers.mixin;

import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.item.ItemStack;

import com.biom4st3r.trackers.components.FoundLootComponent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

@Mixin({MobEntity.class})
public class LivingEntityMxn {
    @Inject(
        at = @At(
            value = "INVOKE",
            target = "net/minecraft/entity/mob/MobEntity.dropStack(Lnet/minecraft/item/ItemStack;)Lnet/minecraft/entity/ItemEntity;", 
            shift = Shift.BEFORE),
        method = "dropEquipment",
        locals = LocalCapture.CAPTURE_FAILHARD)
    private void addFoundLoot_dropEquipment(DamageSource source, int lootingMultiplier, boolean allowDrops, 
            CallbackInfo ci,  EquipmentSlot var4[], int var5, int var6, 
            EquipmentSlot equipmentSlot, ItemStack itemStack) {
        FoundLootComponent.intercept(itemStack, FoundLootComponent.generate((LivingEntity)(Object)this, source));
    }
}
