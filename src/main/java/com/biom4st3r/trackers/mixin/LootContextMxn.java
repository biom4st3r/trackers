package com.biom4st3r.trackers.mixin;

import com.biom4st3r.trackers.duck.LootContextExtras;

import org.spongepowered.asm.mixin.Mixin;

import net.minecraft.loot.context.LootContext;
import net.minecraft.loot.context.LootContextType;

@Mixin({LootContext.class})
public class LootContextMxn implements LootContextExtras {
    LootContextType lootContextExtras$type;
    @Override
    public LootContextType getType() {
        return lootContextExtras$type;
    }

    @Override
    public void setType(LootContextType type) {
        lootContextExtras$type = type;
    }
}
