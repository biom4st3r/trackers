package com.biom4st3r.trackers;

import java.util.function.Consumer;

import net.minecraft.item.ItemStack;
import net.minecraft.loot.condition.LootCondition;
import net.minecraft.loot.context.LootContext;
import net.minecraft.loot.entry.LeafEntry;
import net.minecraft.loot.entry.LootPoolEntryType;
import net.minecraft.loot.function.LootFunction;
import net.minecraft.nbt.NbtCompound;

public class StackEntry extends LeafEntry {
    
    private NbtCompound tag;
    protected StackEntry(ItemStack is, int weight, int quality, LootCondition[] conditions, LootFunction[] functions) {
        super(weight, quality, conditions, functions);
        this.tag = is.writeNbt(new NbtCompound());
    }

    @Override
    protected void generateLoot(Consumer<ItemStack> lootConsumer, LootContext context) {
        lootConsumer.accept(ItemStack.fromNbt(tag));
    }

    public static LeafEntry.Builder<?> builder(ItemStack is) {
        return builder((weight,qunaity,conditions,functions)-> {
            return new StackEntry(is,weight,qunaity,conditions,functions);
        });
    }

    @Override
    public LootPoolEntryType getType() {
        return null;
    }
    
}