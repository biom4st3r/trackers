package com.biom4st3r.trackers.items;

import java.util.List;

import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.EntityType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import net.minecraft.util.Rarity;
import net.minecraft.world.World;

import com.biom4st3r.trackers.ModInit;
import com.biom4st3r.trackers.components.EntryKillComponent;

/**
 * TrackerItem
 */
public class KillTrackerItem extends Item implements TrackerItem {
    public KillTrackerItem() {
        super(new Settings().rarity(Rarity.RARE).maxCount(16).group(ModInit.killTrackersGroup));
    }

    @Override
    public void appendTooltip(ItemStack stack, World world, List<Text> tooltip, TooltipContext context) {
        EntryKillComponent.KEY.borrowPooledComponent(stack, c-> {
            EntityType<?> b = c.get();
            if(b != null) {
                tooltip.add(new TranslatableText(c.get().getTranslationKey()).formatted(Formatting.GRAY));
            }
        }, false);
    }
}