package com.biom4st3r.trackers;

import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;
import net.minecraft.client.render.item.ItemModels;

public interface OnItemModelReload {
    static Event<OnItemModelReload> EVENT = 
    EventFactory.createArrayBacked(OnItemModelReload.class, (listeners)->(models)->
    {
        for(OnItemModelReload thing : listeners) thing.onReload(models);
    });
    void onReload(ItemModels models);
}