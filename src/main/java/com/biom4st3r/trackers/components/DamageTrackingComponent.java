package com.biom4st3r.trackers.components;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.stat.Stat;
import net.minecraft.stat.Stats;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;

import com.google.common.collect.Lists;

public class DamageTrackingComponent extends UpdatingComponent {

    public static final ComponentKey<DamageTrackingComponent> KEY = ComponentKey.of(DamageTrackingComponent.class, "tracker:entrykill", DamageTrackingComponent::new);

    long[] damages = createfilled();

    private static long[] resize(long[] la) {
        if(la.length != getProperSize()) {
            la = Arrays.copyOf(la, getProperSize());
        }
        return la;
    }

    private static long[] createfilled() {
        long[] la = new long[getProperSize()];
        Arrays.fill(la, -1);
        return la;
    }

    @Override
    public void fromStack(ItemStack stack) {
        damages = resize(stack.getOrCreateSubTag(KEY.id).getLongArray("damages"));
    }

    public DamageTrackingComponent enable(Attribute att) {
        this.damages[att.ordinal()] = 0;
        return this;
    }

    @Override
    public ComponentKey<? extends PooledItemComponent> getKey() {
        return KEY;
    }

    private boolean isEmpty() {
        for(long i : damages) {
            if(i > 0) return false;
        }
        return true;
    }

    @Override
    public void toStack(ItemStack stack) {
        if (isEmpty()) return;
        NbtCompound tag = stack.getOrCreateSubTag(KEY.id);
        tag.putLongArray("damages", damages);
    }

    @Override
    public void clear() {
        for(int i = 0; i < damages.length; i++) {
            damages[i] = -1L;
        }
    }

    static final Stat<?> ABSORBED_STAT = Stats.CUSTOM.getOrCreateStat(Stats.DAMAGE_ABSORBED);
    static final Stat<?> RESISTED_STAT = Stats.CUSTOM.getOrCreateStat(Stats.DAMAGE_RESISTED);
    static final Stat<?> TAKEN_STAT = Stats.CUSTOM.getOrCreateStat(Stats.DAMAGE_TAKEN);
    static final Stat<?> DEALT_STAT = Stats.CUSTOM.getOrCreateStat(Stats.DAMAGE_DEALT);
    static final Stat<?> BLOCKED_STAT = Stats.CUSTOM.getOrCreateStat(Stats.DAMAGE_BLOCKED_BY_SHIELD);

    private static int getProperSize() {
        return Attribute.values().length - 1; // to account for `INVALID`
    }

    /**
     * ordinal used to index damage. Safe to add elements. 'INVALID' must be the last element. Can not be rearranged.
     */
    public enum Attribute {
        ABSORBED("dialog.trackers.damage.absorbed"),
        RESISTED("dialog.trackers.damage.resisted"),
        TAKEN("dialog.trackers.damage.taken"),
        DEALT("dialog.trackers.damage.dealt"),
        BLOCKED("dialog.trackers.damage.blocked"),
        INVALID("dialog.trackers.damage.invalid"),
        ;

        public String key;
        Attribute(String key) {
            this.key = key;
        }

        public static Attribute get(Stat<?> stat) {
            if(stat.equals(ABSORBED_STAT)) {
                return ABSORBED;
            } else if (stat.equals(RESISTED_STAT)) {
                 return RESISTED;
            } else if (stat.equals(TAKEN_STAT)) {
                return TAKEN;
            } else if (stat.equals(DEALT_STAT)) {
                return DEALT;
            } else {
                return INVALID;
            }
        }
        public boolean invalid() {
            return this == INVALID;
        }
    }

    @Override
    public boolean react(Stat<?> stat, int i) {
        Attribute att = Attribute.get(stat);
        if (att.invalid()) return false;
        if (this.damages[att.ordinal()] > -1) { // if enabled
            this.damages[att.ordinal()] += i;
            return true;
        }
        return false;
    }

    static final long[] DEFAULT_ARRAY = createfilled();

    @Override
    public List<Text> getToolTips() {
        if(Arrays.mismatch(DEFAULT_ARRAY, damages) == -1) {
            return Collections.emptyList(); 
        }
        Attribute[] attr = Attribute.values();
        List<Text> texts = Lists.newArrayListWithCapacity(0);
        for (Attribute att : attr) {
            if(att.invalid()) break;
            if(this.damages[att.ordinal()] > -1) {
                texts.add(new TranslatableText(att.key, this.damages[att.ordinal()]));
            }
        }
        return texts;
    }
}
