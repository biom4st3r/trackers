package com.biom4st3r.trackers.components;

import java.util.List;

import net.minecraft.text.Text;

public abstract class TrackerComponent extends PooledItemComponent {
    static {
        PooledItemComponent.registerSubType(TrackerComponent.class);
    }

    public abstract List<Text> getToolTips();
}
