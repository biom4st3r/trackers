package com.biom4st3r.trackers.components;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ToolItem;
import net.minecraft.item.Wearable;
import net.minecraft.loot.context.LootContext;
import net.minecraft.loot.context.LootContextParameters;
import net.minecraft.loot.context.LootContextType;
import net.minecraft.loot.context.LootContextTypes;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import com.biom4st3r.trackers.duck.LootContextExtras;
import com.biom4st3r.trackers.items.TrackerItem;
import com.biom4st3r.trackers.mixin.AccessLivingEntity;
import org.jetbrains.annotations.Nullable;

public class FoundLootComponent extends TrackerComponent {
    public static final ComponentKey<FoundLootComponent> KEY = ComponentKey.of(FoundLootComponent.class, "trackers:foundloot", FoundLootComponent::new);

    public enum FoundType {
        CHEST("dialog.itemtrackers.finder.chest"),
        PASSIVE_ENTITY("dialog.itemtrackers.finder.passive"),
        MOB("dialog.itemtrackers.finder.mob"),
        ;
        public String text;
        public String named_text;

        FoundType(String s) {
            this.text = s;
            this.named_text = text + ".named";
        }
        Text getText(FoundLootComponent flc) {
            switch(this) {
                case CHEST:
                    return new TranslatableText(text, flc.finder); // "%s Found this in a chest"
                case MOB:
                case PASSIVE_ENTITY:
                    if(flc.entityNickName.isEmpty()) {
                        return new TranslatableText(this.text, flc.finder, new TranslatableText(flc.source.getTranslationKey())); // "%s defeated a %s for this"
                    } else {
                        return new TranslatableText(this.named_text, flc.finder, flc.entityNickName, new TranslatableText(flc.source.getTranslationKey())); // "%s defeated a %s for this"
                    }
                default:
                    throw new RuntimeException();
            }
        }
    }

    @Override
    public List<Text> getToolTips() {
        return finder.isEmpty() ? Collections.emptyList() : Collections.singletonList(this.type.getText(this));
    }

    public static void intercept(ItemStack is, LootContext context) {
        LootContextType type = ((LootContextExtras)context).getType();
        PlayerEntity player = null;
        Entity source = null;
        if (type == LootContextTypes.CHEST && context.get(LootContextParameters.THIS_ENTITY) instanceof PlayerEntity pe) {
            player = pe;
        } else if(type == LootContextTypes.ENTITY) {
            Entity lastDamagePlayer = context.get(LootContextParameters.LAST_DAMAGE_PLAYER);
            Entity killer = context.get(LootContextParameters.KILLER_ENTITY);
            Entity direct_killer = context.get(LootContextParameters.DIRECT_KILLER_ENTITY);
            source = context.get(LootContextParameters.THIS_ENTITY);
            if(direct_killer instanceof PlayerEntity pe) {
                player = pe;
            } else if(killer instanceof PlayerEntity pe) {
                player = pe;
            } else if(lastDamagePlayer instanceof PlayerEntity pe) {
                player = pe;
            }
        }
        if(player != null) {
            PlayerEntity pe = player;
            Entity source0 = source;
            FoundLootComponent.KEY.borrowPooledComponent(is, component->component.initilize(pe, source0), true);
        }
    }

    public static LootContext generate(LivingEntity entity, DamageSource source) {
        boolean causedByPlayer = ((AccessLivingEntity)entity).getPlayerHitTimer() > 0;
        LootContext.Builder builder = ((AccessLivingEntity)entity).invoke_getLootContextBuilder(causedByPlayer, source);
        return builder.build(LootContextTypes.ENTITY);
    }

    String finder = "";
    long time = -1;
    FoundType type = null;
    EntityType<?> source = null;
    String entityNickName = null;

    public void initilize(PlayerEntity pe, @Nullable Entity e) {
        if(finder.isEmpty()) {
            finder = pe.getEntityName();
            time = new Date().getTime();
            if(e == null) {
                type = FoundType.CHEST;
            } else {
                type = e.getType().getSpawnGroup() == SpawnGroup.MONSTER ? FoundType.MOB : FoundType.PASSIVE_ENTITY;
                source = e.getType();
                entityNickName = e.hasCustomName() ? e.getCustomName().asString() : "";
            }
        }
    }

    @Override
    public void fromStack(ItemStack stack) {
        NbtCompound tag = stack.getOrCreateSubTag(KEY.id);
        finder = tag.getString("finder");
        time = tag.getLong("time");
        type = FoundType.values()[tag.getByte("type")];
        source = Registry.ENTITY_TYPE.get(new Identifier(tag.getString("source")));
    }

    @Override
    public void toStack(ItemStack stack) {
        if (this.finder.isEmpty()) return;
        NbtCompound tag = stack.getOrCreateSubTag(KEY.id);
        tag.putString("finder", finder);
        tag.putLong("time", time);
        tag.putByte("type", (byte) (type != null ? type.ordinal() : 0));
        tag.putString("source", Registry.ENTITY_TYPE.getId(source).toString());
    }

    @Override
    public void clear() {
        finder = "";
        time = -1;
        type = null;
        source = null;
    }

    @Override
    public ComponentKey<? extends PooledItemComponent> getKey() {
        return KEY;
    }

    @Override
    public boolean validItem(ItemStack i) {
        // return true;
        Item item = i.getItem();
        return item.isDamageable() || item instanceof Wearable || item instanceof ToolItem || item instanceof ArmorItem || item instanceof TrackerItem;
    }
    
}
